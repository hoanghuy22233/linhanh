import 'package:json_annotation/json_annotation.dart';

part 'forgot_password_reset_request.g.dart';
@JsonSerializable()
class ForgotPasswordResetRequest {
  @JsonKey(name: "username")
  final String emailOrPhone;
  @JsonKey(name: "otpCode")
  final String otpCode;
  final String password;
  @JsonKey(name: "password_again")
  final String confirmPassword;


  ForgotPasswordResetRequest(
      {this.emailOrPhone, this.otpCode, this.password, this.confirmPassword});

  factory ForgotPasswordResetRequest.fromJson(Map<String, dynamic> json) =>
      _$ForgotPasswordResetRequestFromJson(json);

  Map<String, dynamic> toJson() => _$ForgotPasswordResetRequestToJson(this);
}