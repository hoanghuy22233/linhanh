// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forgot_password_reset_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ForgotPasswordResetRequest _$ForgotPasswordResetRequestFromJson(
    Map<String, dynamic> json) {
  return ForgotPasswordResetRequest(
    emailOrPhone: json['username'] as String,
    otpCode: json['otpCode'] as String,
    password: json['password'] as String,
    confirmPassword: json['password_again'] as String,
  );
}

Map<String, dynamic> _$ForgotPasswordResetRequestToJson(
        ForgotPasswordResetRequest instance) =>
    <String, dynamic>{
      'username': instance.emailOrPhone,
      'otpCode': instance.otpCode,
      'password': instance.password,
      'password_again': instance.confirmPassword,
    };
