// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_product_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewProductResponse _$NewProductResponseFromJson(Map<String, dynamic> json) {
  return NewProductResponse(
    json['data'] == null
        ? null
        : NewsProductData.fromJson(json['data'] as Map<String, dynamic>),
  )
    ..status = json['status'] as int
    ..message = json['message'] as String;
}

Map<String, dynamic> _$NewProductResponseToJson(NewProductResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };
