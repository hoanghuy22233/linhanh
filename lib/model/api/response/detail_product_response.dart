import 'package:applinhanh/model/api/response/base_response.dart';
import 'package:applinhanh/model/entity/detail_product.dart';
import 'package:applinhanh/model/entity/detail_product_data.dart';
import 'package:applinhanh/model/entity/news_product_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'detail_product_response.g.dart';

@JsonSerializable()
class DetailProductResponse extends BaseResponse {
  DetailProductData data;

  DetailProductResponse(this.data);

  factory DetailProductResponse.fromJson(Map<String, dynamic> json) =>
      _$DetailProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DetailProductResponseToJson(this);
}
