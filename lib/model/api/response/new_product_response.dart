import 'package:applinhanh/model/api/response/base_response.dart';
import 'package:applinhanh/model/entity/news_product_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'new_product_response.g.dart';

@JsonSerializable()
class NewProductResponse extends BaseResponse {
  NewsProductData data;

  NewProductResponse(this.data);

  factory NewProductResponse.fromJson(Map<String, dynamic> json) =>
      _$NewProductResponseFromJson(json);

  Map<String, dynamic> toJson() => _$NewProductResponseToJson(this);
}
