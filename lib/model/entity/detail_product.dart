import 'package:applinhanh/model/entity/attr_color.dart';
import 'package:applinhanh/model/entity/attr_quantity.dart';
import 'package:applinhanh/model/entity/attr_size.dart';
import 'package:applinhanh/model/entity/barrel_entity.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'detail_product.g.dart';

@JsonSerializable()
class DetailProduct extends Equatable {
  int id;
  @JsonKey(name: "product_code")
  String productCode;
  String name;
  @JsonKey(name: "price_old")
  double priceOld;
  @JsonKey(name: "price_agency_one")
  double priceAgencyOne;
  @JsonKey(name: "price_agency_two")
  double priceAgencyTwo;
  @JsonKey(name: "total_qty")
  int totalQty;
  @JsonKey(name: "description")
  String description;
  @JsonKey(name: "user_manual")
  String userManual;
  @JsonKey(name: "is_flash_deal")
  double isFlashDeal;
  @JsonKey(name: "percent_off")
  double percentOff;
  @JsonKey(name: "origin_name")
  double originName;
  @JsonKey(name: "manufacturer")
  int manufacturer;
  @JsonKey(name: "image")
  List<String> image;
  @JsonKey(name: "product_attributes")
  dynamic productAttributes;
  @JsonKey(name: "user_type")
  int userType;
  @JsonKey(name: "total_review")
  int totalReview;
  @JsonKey(name: "is_like")
  int isLike;
  @JsonKey(name: "total_rating")
  int totalRating;
  @JsonKey(name: "number_of_products_sold")
  int numberOfProductsSold;
  @JsonKey(name: "average_number_of_stars")
  double averageNumberOfStars;

  DetailProduct(
      this.id,
      this.productCode,
      this.name,
      this.priceOld,
      this.priceAgencyOne,
      this.priceAgencyTwo,
      this.totalQty,
      this.description,
      this.userManual,
      this.isFlashDeal,
      this.percentOff,
      this.originName,
      this.manufacturer,
      this.image,
      this.productAttributes,
      this.userType,
      this.totalReview,
      this.isLike,
      this.totalRating,
      this.numberOfProductsSold,
      this.averageNumberOfStars
      );

  factory DetailProduct.fromJson(Map<String, dynamic> json) =>
      _$DetailProductFromJson(json);

  Map<String, dynamic> toJson() => _$DetailProductToJson(this);

  @override
  List<Object> get props => [
    id,
    productCode,
    name,
    priceOld,
    priceAgencyOne,
    priceAgencyTwo,
    totalQty,
    description,
    userManual,
    isFlashDeal,
    percentOff,
    originName,
    manufacturer,
    image,
    productAttributes,
    userType,
    totalReview,
    isLike,
    totalRating,
    numberOfProductsSold,
    averageNumberOfStars
      ];

  @override
  String toString() {
    return 'Detail product{id: $id, name: $name, productCode: $productCode, priceOld: $priceOld,    priceAgencyOne: $priceAgencyOne,  priceAgencyTwo:  $priceAgencyTwo,  totalQty:  $totalQty,  description:  $description,  userManual:  $userManual,  isFlashDeal:  $isFlashDeal,  percentOff:  $percentOff,originName: $originName, manufacturer: $manufacturer, image: $image, productAttributes: $productAttributes,userType: $userType,totalReview: $totalReview, isLike: $isLike, totalRating: $totalRating,numberOfProductsSold: $numberOfProductsSold,averageNumberOfStars: $averageNumberOfStars}';  }
}
