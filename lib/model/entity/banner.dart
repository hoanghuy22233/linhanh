import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';

part 'banner.g.dart';

@JsonSerializable()
class Banner extends Equatable {
  String image;
  int type;

  Banner({this.image, this.type});

  factory Banner.fromJson(Map<String, dynamic> json) => _$BannerFromJson(json);

  Map<String, dynamic> toJson() => _$BannerToJson(this);

  @override
  List<Object> get props => [image, type];

  @override
  String toString() {
    return 'Banner{image: $image, type: $type}';
  }
}
