// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_product_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailProductData _$DetailProductDataFromJson(Map<String, dynamic> json) {
  return DetailProductData(
    json['product'] == null
        ? null
        : DetailProduct.fromJson(json['product'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DetailProductDataToJson(DetailProductData instance) =>
    <String, dynamic>{
      'product': instance.product,
    };
