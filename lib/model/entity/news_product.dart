import 'package:applinhanh/model/entity/attr_color.dart';
import 'package:applinhanh/model/entity/attr_quantity.dart';
import 'package:applinhanh/model/entity/attr_size.dart';
import 'package:applinhanh/model/entity/barrel_entity.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'news_product.g.dart';

@JsonSerializable()
class NewsProduct extends Equatable {
  int id;
  @JsonKey(name: "product_code")
  String productCode;
  String name;
  @JsonKey(name: "slug")
  String slug;
  @JsonKey(name: "import_price")
  double importPrice;
  @JsonKey(name: "price_old")
  double priceOld;
  @JsonKey(name: "price_new")
  double priceNew;
  @JsonKey(name: "price_agency_one")
  double priceAgencyOne;
  @JsonKey(name: "price_agency_two")
  double priceAgencyTwo;
  @JsonKey(name: "total_qty")
  int totalQty;
  @JsonKey(name: "image")
  List<String> image;
  @JsonKey(name: "description")
  String description;
  @JsonKey(name: "user_manual")
  String userManual;
  @JsonKey(name: "title_seo")
  String titleSeo;
  @JsonKey(name: "description_seo")
  String descriptionSeo;
  @JsonKey(name: "keyword_seo")
    double keywordSeo;
  @JsonKey(name: "is_flash_deal")
    double isFlashDeal;
  @JsonKey(name: "is_sale_off")
    double isSaleOff;
  @JsonKey(name: "percent_off")
  double percentOff;
  @JsonKey(name: "manufacturer_id")
  int manufacturerId;
  @JsonKey(name: "supplier_id")
  String supplierId;
  @JsonKey(name: "status")
  int status;
  @JsonKey(name: "created_at")
  String createdAt;
  @JsonKey(name: "updated_at")
  String updatedAt;
  @JsonKey(name: "parent_id")
  int parentId;
  @JsonKey(name: "manufacturer_name")
  String manufacturerName;
  @JsonKey(name: "user_type")
  int userType;
  @JsonKey(name: "price_sale")
  double priceSale;
  @JsonKey(name: "is_like")
  int isLike;
  @JsonKey(name: "totalQuantity")
  int totalQuantity;
  @JsonKey(name: "productsSoldNumber")
  int productsSoldNumber;

  NewsProduct(
    this.id,
    this.productCode,
    this.name,
    this.slug,
    this.importPrice,
    this.priceOld,
    this.priceNew,
    this.priceAgencyOne,
    this.priceAgencyTwo,
    this.totalQty,
    this.image,
    this.description,
    this.userManual,
    this.titleSeo,
    this.descriptionSeo,
    this.keywordSeo,
    this.isFlashDeal,
    this.isSaleOff,
    this.percentOff,
    this.manufacturerId,
    this.supplierId,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.parentId,
    this.manufacturerName,
    this.userType,
    this.priceSale,
    this.isLike,
    this.totalQuantity,
    this.productsSoldNumber
);

  factory NewsProduct.fromJson(Map<String, dynamic> json) =>
      _$NewsProductFromJson(json);

  Map<String, dynamic> toJson() => _$NewsProductToJson(this);

  @override
  List<Object> get props => [
    id,
    productCode,
    name,
    slug,
    importPrice,
    priceOld,
    priceNew,
    priceAgencyOne,
    priceAgencyTwo,
    totalQty,
    image,
    description,
    userManual,
    titleSeo,
    descriptionSeo,
    keywordSeo,
    isFlashDeal,
    isSaleOff,
    percentOff,
    manufacturerId,
    supplierId,
    status,
    createdAt,
    updatedAt,
    parentId,
    manufacturerName,
    userType,
    priceSale,
    isLike,
    totalQuantity,
    productsSoldNumber
      ];

  @override
  String toString() {
    return 'Product{id: $id, name: $name, productCode: $productCode, $slug,$importPrice, $priceOld, $priceNew, $priceAgencyOne,$priceAgencyTwo,    $totalQty,    $image,    $description,    $userManual,    $titleSeo,    $descriptionSeo,    $keywordSeo,    $isFlashDeal,    $isSaleOff,  $percentOff,    $manufacturerId,    $supplierId,    $status,    $createdAt,    $updatedAt,    $parentId,   $manufacturerName,    $userType,    $priceSale,    $isLike,    $totalQuantity,    $productsSoldNumber}';
  }
}
