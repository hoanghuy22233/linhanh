import 'package:applinhanh/model/entity/barrel_entity.dart';
import 'package:applinhanh/model/entity/detail_product.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'detail_product_data.g.dart';

@JsonSerializable()
class DetailProductData extends Equatable {
  DetailProduct product;

  DetailProductData(this.product);

  factory DetailProductData.fromJson(Map<String, dynamic> json) =>
      _$DetailProductDataFromJson(json);

  Map<String, dynamic> toJson() => _$DetailProductDataToJson(this);

  @override
  List<Object> get props => [product];
}
