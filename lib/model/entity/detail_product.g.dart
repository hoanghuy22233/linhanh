// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DetailProduct _$DetailProductFromJson(Map<String, dynamic> json) {
  return DetailProduct(
    json['id'] as int,
    json['product_code'] as String,
    json['name'] as String,
    (json['price_old'] as num)?.toDouble(),
    (json['price_agency_one'] as num)?.toDouble(),
    (json['price_agency_two'] as num)?.toDouble(),
    json['total_qty'] as int,
    json['description'] as String,
    json['user_manual'] as String,
    (json['is_flash_deal'] as num)?.toDouble(),
    (json['percent_off'] as num)?.toDouble(),
    (json['origin_name'] as num)?.toDouble(),
    json['manufacturer'] as int,
    (json['image'] as List)?.map((e) => e as String)?.toList(),
    json['product_attributes'],
    json['user_type'] as int,
    json['total_review'] as int,
    json['is_like'] as int,
    json['total_rating'] as int,
    json['number_of_products_sold'] as int,
    (json['average_number_of_stars'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$DetailProductToJson(DetailProduct instance) =>
    <String, dynamic>{
      'id': instance.id,
      'product_code': instance.productCode,
      'name': instance.name,
      'price_old': instance.priceOld,
      'price_agency_one': instance.priceAgencyOne,
      'price_agency_two': instance.priceAgencyTwo,
      'total_qty': instance.totalQty,
      'description': instance.description,
      'user_manual': instance.userManual,
      'is_flash_deal': instance.isFlashDeal,
      'percent_off': instance.percentOff,
      'origin_name': instance.originName,
      'manufacturer': instance.manufacturer,
      'image': instance.image,
      'product_attributes': instance.productAttributes,
      'user_type': instance.userType,
      'total_review': instance.totalReview,
      'is_like': instance.isLike,
      'total_rating': instance.totalRating,
      'number_of_products_sold': instance.numberOfProductsSold,
      'average_number_of_stars': instance.averageNumberOfStars,
    };
