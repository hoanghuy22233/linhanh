// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_product_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewsProductData _$NewsProductDataFromJson(Map<String, dynamic> json) {
  return NewsProductData(
    (json['rows'] as List)
        ?.map((e) =>
            e == null ? null : NewsProduct.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$NewsProductDataToJson(NewsProductData instance) =>
    <String, dynamic>{
      'rows': instance.rows,
    };
