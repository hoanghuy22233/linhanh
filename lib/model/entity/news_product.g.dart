// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewsProduct _$NewsProductFromJson(Map<String, dynamic> json) {
  return NewsProduct(
    json['id'] as int,
    json['product_code'] as String,
    json['name'] as String,
    json['slug'] as String,
    (json['import_price'] as num)?.toDouble(),
    (json['price_old'] as num)?.toDouble(),
    (json['price_new'] as num)?.toDouble(),
    (json['price_agency_one'] as num)?.toDouble(),
    (json['price_agency_two'] as num)?.toDouble(),
    json['total_qty'] as int,
    (json['image'] as List)?.map((e) => e as String)?.toList(),
    json['description'] as String,
    json['user_manual'] as String,
    json['title_seo'] as String,
    json['description_seo'] as String,
    (json['keyword_seo'] as num)?.toDouble(),
    (json['is_flash_deal'] as num)?.toDouble(),
    (json['is_sale_off'] as num)?.toDouble(),
    (json['percent_off'] as num)?.toDouble(),
    json['manufacturer_id'] as int,
    json['supplier_id'] as String,
    json['status'] as int,
    json['created_at'] as String,
    json['updated_at'] as String,
    json['parent_id'] as int,
    json['manufacturer_name'] as String,
    json['user_type'] as int,
    (json['price_sale'] as num)?.toDouble(),
    json['is_like'] as int,
    json['totalQuantity'] as int,
    json['productsSoldNumber'] as int,
  );
}

Map<String, dynamic> _$NewsProductToJson(NewsProduct instance) =>
    <String, dynamic>{
      'id': instance.id,
      'product_code': instance.productCode,
      'name': instance.name,
      'slug': instance.slug,
      'import_price': instance.importPrice,
      'price_old': instance.priceOld,
      'price_new': instance.priceNew,
      'price_agency_one': instance.priceAgencyOne,
      'price_agency_two': instance.priceAgencyTwo,
      'total_qty': instance.totalQty,
      'image': instance.image,
      'description': instance.description,
      'user_manual': instance.userManual,
      'title_seo': instance.titleSeo,
      'description_seo': instance.descriptionSeo,
      'keyword_seo': instance.keywordSeo,
      'is_flash_deal': instance.isFlashDeal,
      'is_sale_off': instance.isSaleOff,
      'percent_off': instance.percentOff,
      'manufacturer_id': instance.manufacturerId,
      'supplier_id': instance.supplierId,
      'status': instance.status,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'parent_id': instance.parentId,
      'manufacturer_name': instance.manufacturerName,
      'user_type': instance.userType,
      'price_sale': instance.priceSale,
      'is_like': instance.isLike,
      'totalQuantity': instance.totalQuantity,
      'productsSoldNumber': instance.productsSoldNumber,
    };
