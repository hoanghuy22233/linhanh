import 'package:applinhanh/model/entity/barrel_entity.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'news_product_data.g.dart';

@JsonSerializable()
class NewsProductData extends Equatable {
  List<NewsProduct> rows;

  NewsProductData(this.rows);

  factory NewsProductData.fromJson(Map<String, dynamic> json) =>
      _$NewsProductDataFromJson(json);

  Map<String, dynamic> toJson() => _$NewsProductDataToJson(this);

  @override
  List<Object> get props => [rows];
}
