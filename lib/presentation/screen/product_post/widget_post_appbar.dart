import 'package:applinhanh/presentation/common_widgets/widget_appbar.dart';
import 'package:applinhanh/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:flutter/material.dart';

class WidgetPostAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: WidgetAppbar(
        hasIndicator: true,
        left: [WidgetAppbarMenuBack()],
        title: "Tạo bài viết",
      ),
    );
  }
}
