import 'dart:io';

import 'package:applinhanh/app/constants/endpoint/app_endpoint.dart';
import 'package:applinhanh/app/constants/navigator/navigator.dart';
import 'package:applinhanh/model/repo/user_repository.dart';
import 'package:applinhanh/presentation/screen/product_post/product_post_form/bloc/post_form_event.dart';
import 'package:applinhanh/presentation/screen/product_post/product_post_form/bloc/post_form_state.dart';
import 'package:applinhanh/utils/dio/dio_error_util.dart';
import 'package:applinhanh/utils/dio/dio_status.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

class PostFormBloc extends Bloc<PostFormEvent, PostFormState> {
  final UserRepository _userRepository;

  PostFormBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  PostFormState get initialState => PostFormState.empty();

  @override
  Stream<PostFormState> mapEventToState(PostFormEvent event) async* {
    if (event is InitPostForm) {
      yield* _mapInitPostFormToState();
    } else if (event is ImageLoaded) {
      yield* _mapLoadImageLoadedToState(event.image);
    } else if (event is NamesChanged) {
      yield* _mapNamesChangedToState(event.name);
    } else if (event is PostFormSubmitted) {
      yield* _mapPostFormSubmittedToState();
    }
  }

  Stream<PostFormState> _mapInitPostFormToState() async* {
    yield PostFormState.empty();
  }

  Stream<PostFormState> _mapLoadImageLoadedToState(List<File> images) async* {
    yield state.update(images: images);
  }

  Stream<PostFormState> _mapNamesChangedToState(String name) async* {
    yield state.update(name: name);
  }

  Stream<PostFormState> _mapPostFormSubmittedToState() async* {
    try {
      if (state.checkReadyToOrder()) {
        yield PostFormState.loading(state.copyWith(
            status:
                DioStatus(message: '', code: DioStatus.API_PROGRESS_NOTIFY)));

        var response = await _userRepository.postProduct(
          storeImages: state.images,
          name: state.name,
        );

        if (response.status == Endpoint.SUCCESS) {
          yield PostFormState.success(state.copyWith(
              status: DioStatus(
                  message: response.message,
                  code: DioStatus.API_SUCCESS_NOTIFY)));
          Future.delayed(Duration(seconds: 2), () {
            AppNavigator.navigateNavigation();
          });
        } else {
          yield PostFormState.failure(state.copyWith(
              status: DioStatus(
                  message: response.message,
                  code: DioStatus.API_FAILURE_NOTIFY)));
        }
      }
    } catch (e) {
      yield PostFormState.failure(
          state.update(status: DioErrorUtil.handleError(e)));
    }
  }
}
