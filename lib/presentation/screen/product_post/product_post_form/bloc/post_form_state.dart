import 'dart:io';

import 'package:applinhanh/utils/dio/dio_status.dart';

class PostFormState {
  final List<File> images;
  final String name;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final DioStatus status;

  PostFormState(
      {this.images,
      this.name,
      this.isSubmitting,
      this.isSuccess,
      this.isFailure,
      this.status});

  factory PostFormState.empty() {
    return PostFormState(
        images: null,
        name: null,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        status: null);
  }

  factory PostFormState.loading(PostFormState state) {
    return PostFormState(
        images: state.images,
        name: state.name,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
        status: state.status);
  }

  factory PostFormState.failure(PostFormState state) {
    return PostFormState(
        images: state.images,
        name: state.name,
        isSuccess: false,
        isSubmitting: false,
        isFailure: true,
        status: state.status);
  }

  factory PostFormState.success(PostFormState state) {
    return PostFormState(
        images: state.images,
        name: state.name,
        isSuccess: true,
        isSubmitting: false,
        isFailure: false,
        status: state.status);
  }

  PostFormState update(
      {List<File> images,
      String name,
      bool isSubmitting,
      bool isSuccess,
      bool isFailure,
      DioStatus status}) {
    return copyWith(
        images: images,
        name: name,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        status: status);
  }

  PostFormState copyWith({
    List<File> images,
    String name,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    DioStatus status,
  }) {
    return PostFormState(
      images: images ?? this.images,
      name: name ?? this.name,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      status: status ?? this.status,
    );
  }

  @override
  String toString() {
    return 'PostFormState{images: $images, name: $name,  isSubmitting: $isSubmitting, isSuccess: $isSuccess, isFailure: $isFailure, status: $status}';
  }

  bool checkReadyToOrder() {
    var ready = true;

//    if (address == null) {
//      ready = false;
//    }
//    if (wallet == null) {
//      ready = false;
//    }
    return ready;
  }
}
