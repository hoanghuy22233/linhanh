import 'dart:io';

import 'package:applinhanh/app/constants/color/color.dart';
import 'package:applinhanh/presentation/common_widgets/widget_login_button.dart';
import 'package:applinhanh/presentation/common_widgets/widget_spacer.dart';
import 'package:applinhanh/presentation/screen/product_post/product_post_form/widget_form_post_input.dart';
import 'package:applinhanh/utils/handler/http_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

import 'bloc/post_form_bloc.dart';
import 'bloc/post_form_event.dart';
import 'bloc/post_form_state.dart';

class WidgetPostsForm extends StatefulWidget {
  @override
  _WidgetPostsFormState createState() => _WidgetPostsFormState();
}

class _WidgetPostsFormState extends State<WidgetPostsForm> {
  PostFormBloc _PostFormBloc;

  List<File> _images = [];
  final ImagePicker picker = ImagePicker();
  List image = List();

  TextEditingController _nameTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _PostFormBloc = BlocProvider.of<PostFormBloc>(context);
    _nameTextController.addListener(_onNameChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PostFormBloc, PostFormState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          print('---isSubmitting---');
          print(state.status);
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isSuccess) {
          print('---isSuccess---');
          print(state.status);
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          print('---isFailure---');
          print(state.status);
          await HttpHandler.resolve(status: state.status);
        }
      },
      child:
          BlocBuilder<PostFormBloc, PostFormState>(builder: (context, state) {
        return Container(
          color: AppColor.GREY_LIGHTER_3,
          child: Form(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  color: AppColor.WHITE,
                  // padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      WidgetSpacer(
                        height: 10,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          "Tạo bài viết",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          // _onPickAvatar();
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: GridView.builder(
                              shrinkWrap: true,
                              padding:
                                  EdgeInsets.only(top: 10, left: 5, right: 5),
                              itemBuilder: (context, index) {
                                if (index == _images.length) {
                                  return GestureDetector(
                                    onTap: () {
                                      getImageFromGallery();
                                    },
                                    child: Container(
                                      padding: EdgeInsets.only(left: 12),
                                      height: 150,
                                      child: Image.asset(
                                          "assets/images/img_camera2.png"),
                                    ),
                                  );
                                }
                                return Stack(
                                  alignment: Alignment.topRight,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      height: 160,
                                      width: 160,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Image.file(
                                          _images[index],
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            _images.removeAt(index);
                                          });
                                          imageCache.clear();
                                        },
                                        child: Image.asset(
                                          "assets/images/img_close_round.png",
                                          height: 25,
                                          width: 25,
                                        ),
                                      ),
                                    )
                                  ],
                                );
                              },
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                              ),
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: _images.length + 1),
                        ),
                      ),
                      Container(
                        child: Divider(
                          height: 1,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: WidgetFormInput(
                          controller: _nameTextController,
                          text: '',
                          hint: "Bạn đang nghĩ gì?",
                          // validator: _productPostFormBloc.state.name.isNotEmpty?? '',
                        ),
                      ),
                      Container(
                        child: Divider(
                          height: 1,
                        ),
                      ),
                    ],
                  ),
                ),
//                Container(
//                  padding: EdgeInsets.symmetric(horizontal: 10),
//                  width: double.infinity,
//                  color: AppColor.WHITE,
//                  child: WidgetPostUnitsType(
//                  ),
//                ),
                Container(
                  width: double.infinity,
                  color: AppColor.WHITE,
                  padding: EdgeInsets.all(8.0),
                  child: _buildSubmitButton(),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  _buildSubmitButton() {
    return WidgetLoginButton(
      onTap: () {
        if (_PostFormBloc.state.checkReadyToOrder()) {
          _PostFormBloc.add(PostFormSubmitted());
        }
      },
      isEnable: _PostFormBloc.state.checkReadyToOrder(),
      text: "Đăng",
    );
  }

  void _onNameChange() {
    _PostFormBloc.add(NamesChanged(name: _nameTextController.text));
  }

  Future getImageFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null && pickedFile.path != "") {
      setState(() {
        _images.add(File(pickedFile.path));
      });
      BlocProvider.of<PostFormBloc>(context).add(ImageLoaded(image: _images));
    }
  }

  @override
  void dispose() {
    _nameTextController.dispose();
    super.dispose();
  }
}
