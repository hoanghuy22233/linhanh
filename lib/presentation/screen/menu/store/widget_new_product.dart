import 'dart:async';
import 'package:applinhanh/app/constants/barrel_constants.dart';
import 'package:applinhanh/app/constants/style/style.dart';
import 'package:applinhanh/utils/handler/http_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';

import 'bloc/new_product_bloc.dart';
import 'bloc/new_product_state.dart';


class WidgetNewProductListItem extends StatefulWidget {
  @override
  _WidgetNewProductListItemState createState() => _WidgetNewProductListItemState();
}

class _WidgetNewProductListItemState extends State<WidgetNewProductListItem> {
  bool isLoading = true;

  @override
  Widget build(BuildContext context) {
    Timer timer = Timer(Duration(seconds: 3), () {
      setState(() {
        isLoading = false;
      });
    });
    return BlocListener<NewProductBloc, NewProductState>(
      listener: (context, state) async {
        if (state.isLoading) {
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isSuccess) {
          await HttpHandler.resolve(status: state.status);
          print("_____________");
          print(state.status);
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
          print(state.status);
        }
      },
      child: BlocBuilder<NewProductBloc, NewProductState>(
        builder: (context, state) {
          return _buildContent(state);
        },
      ),
    );
  }

  Widget _buildContent(NewProductState state) {
    if (state?.newProduct?.length != 0 && state.newProduct != null) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: GridView.builder(
            itemCount: state.newProduct.length,
            padding: const EdgeInsets.all(0.0),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, // Number of items in row
              childAspectRatio: 0.6,
              crossAxisSpacing: 10.0, // Space between columns
              mainAxisSpacing: 10, // Space between rows
            ),
            itemBuilder: (context, index) {
              // Listen to specific object
              return GestureDetector(
                onTap: (){
                  AppNavigator.navigateDetailProduct(state.newProduct[index].id);
                },
                child: _newProductItem(state.newProduct[index].image[0], state.newProduct[index].priceAgencyOne, state.newProduct[index].name),
              );
            }),
      );
    } else {
      return isLoading
          ? ShimmerList()
          : Center(child: Text("Không có sản phẩm!"));
    }
  }

}
Widget _newProductItem(String image, double price, String name){
  return Container(
    decoration: BoxDecoration(
      border: Border.all(color: Colors.grey[400], width: 0.3),
      color: Colors.white
    ),
    child: Column(
      children: [
        Image.network(image, height: 150, width: 150,),
        SizedBox(height: 20,),
        Text(AppValue.APP_MONEY_FORMAT.format(price), style: AppStyle.APP_MEDIUM.copyWith(color: Colors.green[600]),),
        SizedBox(height: 20,),
        Text(name, style: AppStyle.APP_MEDIUM_BOLD.copyWith(color: Colors.red[600]), textAlign: TextAlign.center,),
      ],
    ),
  );
}

class ShimmerList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int offset = 0;
    int time = 800;

    return SafeArea(
      child: ListView.builder(
        itemCount: 6,
        itemBuilder: (BuildContext context, int index) {
          offset += 5;
          time = 800 + offset;

          print(time);

          return Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Shimmer.fromColors(
                highlightColor: Colors.white,
                baseColor: Colors.grey[300],
                child: ShimmerLayout(),
                period: Duration(milliseconds: time),
              ));
        },
      ),
    );
  }
}

class ShimmerLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double containerWidth = 280;
    double containerHeight = 15;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 7.5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            height: 100,
            width: 100,
            color: Colors.grey,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: containerHeight,
                width: containerWidth,
                color: Colors.grey,
              ),
              SizedBox(height: 5),
              Container(
                height: containerHeight,
                width: containerWidth,
                color: Colors.grey,
              ),
              SizedBox(height: 5),
              Container(
                height: containerHeight,
                width: containerWidth * 0.75,
                color: Colors.grey,
              )
            ],
          )
        ],
      ),
    );
  }
}

class ShimmerImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 500,
        width: 500,
        child: Shimmer.fromColors(
          baseColor: Colors.black,
          highlightColor: Colors.white,
          child: Image.asset("thecsguy.PNG"),
          period: Duration(seconds: 3),
        ),
      ),
    );
  }
}

