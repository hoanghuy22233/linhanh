import 'package:applinhanh/app/constants/barrel_constants.dart';
import 'package:applinhanh/app/constants/color/color.dart';
import 'package:applinhanh/presentation/screen/menu/new/bloc/post_bloc.dart';
import 'package:applinhanh/presentation/screen/menu/new/bloc/post_event.dart';
import 'package:applinhanh/presentation/screen/menu/new/widget_posts.dart';
import 'package:applinhanh/presentation/screen/menu/store/widget_new_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/new_product_bloc.dart';
import 'bloc/new_product_event.dart';


class StorePages extends StatefulWidget {
  StorePages({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _StorePages createState() => _StorePages();
}

class _StorePages extends State<StorePages> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<NewProductBloc>(context).add(LoadNewProduct());
  }

  // Widget build(BuildContext context) {
  //   SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
  //     statusBarColor: Colors.red,
  //   ));
  //   return Scaffold(
  //     body: Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: [
  //         Expanded(
  //           child: RefreshIndicator(
  //               onRefresh: () async {
  //                 BlocProvider.of<PostBloc>(context).add(RefreshPost());
  //                 await Future.delayed(Duration(seconds: 2));
  //                 return true;
  //               },
  //               color: AppColor.PRIMARY_COLOR,
  //               backgroundColor: AppColor.THIRD_COLOR,
  //               child: _buildContent()),
  //         ),
  //       ],
  //     ),
  //   ); // This trailing comma makes auto-formatting nicer for build methods.
  // }
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            color: Colors.white
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildBanner(),
            _buildTitle(),
            Expanded(
              child: RefreshIndicator(
                  onRefresh: () async {
                    BlocProvider.of<NewProductBloc>(context).add(RefreshNewProduct());
                    await Future.delayed(Duration(seconds: 2));
                    return true;
                  },
                  color: AppColor.PRIMARY_COLOR,
                  backgroundColor: AppColor.THIRD_COLOR,
                  child: _buildContent()),
            ),
          ],
        ),
      ),
    );
    // This trailing comma makes auto-formatting nicer for build methods.
  }



  Widget _buildBanner(){
    return Container(
      height: 150,
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(),
      child: Image.asset("assets/images/banner_store.jpg"),
    );
  }

  Widget _buildTitle(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("Tất cả sản phẩm", style: AppStyle.APP_MEDIUM_BOLD.copyWith(color: Colors.green[600]),),
        ],
      ),
    );
  }
  Widget _buildContent() => WidgetNewProductListItem();
}
