import 'package:applinhanh/app/constants/color/color.dart';
import 'package:applinhanh/app/constants/value/value.dart';
import 'package:applinhanh/presentation/common_widgets/widget_banner_wrapper.dart';
import 'package:applinhanh/presentation/common_widgets/widget_cached_image.dart';
import 'package:applinhanh/presentation/common_widgets/widget_circle_progress.dart';
import 'package:applinhanh/presentation/common_widgets/widget_screen_error.dart';
import 'package:applinhanh/presentation/screen/menu/store/detail_product/bloc/product_detail_bloc.dart';
import 'package:applinhanh/presentation/screen/menu/store/detail_product/bloc/product_detail_event.dart';
import 'package:applinhanh/presentation/screen/menu/store/detail_product/bloc/product_detail_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import 'detail_product_appbar.dart';

class DetailProductScreen extends StatefulWidget {
  @override
  _DetailProductScreenState createState() => _DetailProductScreenState();
}

class _DetailProductScreenState extends State<DetailProductScreen> {
  int _productID;
  bool isReadDetail = false;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _productID = arguments['idProduct'];
        print('---idProduct---');
        print(_productID);

        BlocProvider.of<DetailProductBloc>(context)
            .add(LoadDetailProduct(_productID));
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
    // BlocProvider.of<DetailProductBloc>(context)
    //     .add(LoadDetailProduct(_newsId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            _buildAppbar(),
            Expanded(
                child: RefreshIndicator(
                    onRefresh: () async {
                      BlocProvider.of<DetailProductBloc>(context)
                          .add(RefreshDetailProduct(_productID));
                      await Future.delayed(AppValue.FAKE_TIME_RELOAD);
                      return true;
                    },
                    color: Colors.black,
                    backgroundColor: Colors.white,
                    child: BlocBuilder<DetailProductBloc, DetailProductState>(
                      builder: (context, state) {
                        return _buildContent(state);
                      },
                    ))),
          ],
        ));
  }

  Widget _buildAppbar() => WidgetNewsDetailAppbar();

  Widget _buildContent(DetailProductState state) {
    if (state is DetailProductLoaded) {
      return SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
//      color: Colors.red,
              height: 300,
              child: WidgetBannerWrapper(
                child: Swiper(
                    autoplay: true,
                    itemCount: state?.products?.product?.image?.length ?? 0,
                    itemBuilder: (context, index) {
                      return WidgetCachedImage(
                        url: state?.products?.product?.image[index] ?? '',
                      );
                    },
                    pagination: new SwiperPagination(
                        builder: DotSwiperPaginationBuilder(
                            size: 5,
                            activeSize: 10,
                            color: AppColor.BANNER_COLOR,
                            activeColor: AppColor.BANNER_SELECTED_COLOR))),
              ),
            ),
            Text(state?.products?.product?.name ?? ''),
          ],
        ),
      );
    } else if (state is DetailProductLoading) {
      return Center(
        child: WidgetCircleProgress(),
      );
    } else if (state is DetailProductNotLoaded) {
      return WidgetScreenError(
        status: state.status,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }
}
