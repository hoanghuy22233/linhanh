import 'package:applinhanh/model/entity/detail_product_data.dart';
import 'package:applinhanh/utils/dio/dio_status.dart';
import 'package:equatable/equatable.dart';

abstract class DetailProductState extends Equatable {
  const DetailProductState();

  @override
  List<Object> get props => [];
}

class DetailProductLoading extends DetailProductState {}

class DetailProductLoaded extends DetailProductState {
  final DetailProductData products;

  const DetailProductLoaded({this.products});

  @override
  List<Object> get props => [products];

  @override
  String toString() {
    return 'DetailProductLoaded{news: $products}';
  }
}

class DetailProductNotLoaded extends DetailProductState {
  final DioStatus status;

  DetailProductNotLoaded(this.status);

  @override
  String toString() {
    return 'DetailProductNotLoaded{error: $status}';
  }
}
