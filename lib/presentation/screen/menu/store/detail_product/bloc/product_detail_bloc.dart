import 'package:applinhanh/model/repo/user_repository.dart';
import 'package:applinhanh/presentation/screen/menu/store/detail_product/bloc/product_detail_event.dart';
import 'package:applinhanh/presentation/screen/menu/store/detail_product/bloc/product_detail_state.dart';
import 'package:applinhanh/utils/dio/dio_error_util.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

class DetailProductBloc extends Bloc<DetailProductEvent, DetailProductState> {
  final UserRepository userRepository;

  DetailProductBloc({@required this.userRepository}) : super();

  @override
  DetailProductState get initialState => DetailProductLoading();

  @override
  Stream<DetailProductState> mapEventToState(DetailProductEvent event) async* {
    if (event is LoadDetailProduct) {
      yield* _mapLoadDetailProductToState(event.productId);
    } else if (event is RefreshDetailProduct) {
      yield DetailProductLoading();
      yield* _mapLoadDetailProductToState(event.productId);
    }
  }

  Stream<DetailProductState> _mapLoadDetailProductToState(
      int productId) async* {
    try {
      final response = await userRepository.detailProduct(productId: productId);
      yield DetailProductLoaded(products: response.data);
    } catch (e) {
      yield DetailProductNotLoaded(DioErrorUtil.handleError(e));
    }
  }
}
