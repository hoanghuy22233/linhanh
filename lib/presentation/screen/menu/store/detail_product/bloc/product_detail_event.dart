import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class DetailProductEvent extends Equatable {
  const DetailProductEvent();

  List<Object> get props => [];
}

class LoadDetailProduct extends DetailProductEvent {
  final int productId;

  LoadDetailProduct(this.productId);

  @override
  String toString() {
    return 'LoadDetailProduct{productId: $productId}';
  }

  List<Object> get props => [productId];
}

class RefreshDetailProduct extends DetailProductEvent {
  final int productId;

  RefreshDetailProduct(this.productId);

  @override
  String toString() {
    return 'RefreshDetailProduct{productId: $productId}';
  }

  List<Object> get props => [productId];
}