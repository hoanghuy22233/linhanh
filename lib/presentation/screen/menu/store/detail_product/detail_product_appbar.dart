import 'package:applinhanh/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:applinhanh/presentation/common_widgets/widget_appbar.dart';
import 'package:flutter/material.dart';

class WidgetNewsDetailAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: WidgetAppbar(
        title: "Chi tiết sản phẩm",
        left: [
          WidgetAppbarMenuBack()
        ],
        indicatorColor: Colors.red,
      ),
    );
  }
}
