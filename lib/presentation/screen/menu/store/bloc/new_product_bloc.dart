import 'package:applinhanh/model/repo/user_repository.dart';

import 'package:applinhanh/utils/dio/dio_error_util.dart';
import 'package:applinhanh/utils/dio/dio_status.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'new_product_event.dart';
import 'new_product_state.dart';

class NewProductBloc extends Bloc<NewProductEvent, NewProductState> {
  final UserRepository userRepository;

  NewProductBloc({@required this.userRepository});

  @override
  NewProductState get initialState => NewProductState.empty();

  @override
  Stream<NewProductState> mapEventToState(NewProductEvent event) async* {
    if (event is LoadNewProduct) {
      yield* _mapLoadNewProductToState();
    } else if (event is RefreshNewProduct) {
      yield NewProductState.loading(state.copyWith(
          status: DioStatus(
        code: DioStatus.API_PROGRESS,
      )));
      yield* _mapLoadNewProductToState();
    }
  }

  Stream<NewProductState> _mapLoadNewProductToState() async* {
    try {
      final response = await userRepository.getNewProduct();
      yield NewProductState.success(state.update(
          newProduct: response.data.rows,
          status: DioStatus(
              code: DioStatus.API_SUCCESS, message: response.message)));
      print("____________________________");
      print(response.data);
      print(response.message);
      print(response.status);
    } catch (e) {
      yield NewProductState.failure(
          state.update(status: DioErrorUtil.handleError(e)));
    }
  }

}
