import 'package:applinhanh/model/entity/news.dart';
import 'package:applinhanh/model/entity/news_product.dart';
import 'package:applinhanh/utils/dio/dio_status.dart';
import 'package:meta/meta.dart';

class NewProductState {
  final List<NewsProduct> newProduct;
  final bool isLoading;
  final bool isSuccess;
  final bool isFailure;
  final DioStatus status;

  NewProductState(
      {@required this.newProduct,
      @required this.isLoading,
      @required this.isSuccess,
      @required this.isFailure,
      @required this.status});

  factory NewProductState.empty() {
    return NewProductState(
        newProduct: null,
        isLoading: false,
        isSuccess: false,
        isFailure: false,
        status: null);
  }

  factory NewProductState.loading(NewProductState state) {
    return NewProductState(
        newProduct: state.newProduct,
        isLoading: true,
        isSuccess: false,
        isFailure: false,
        status: state.status);
  }

  factory NewProductState.failure(NewProductState state) {
    return NewProductState(
        newProduct: state.newProduct,
        isLoading: false,
        isSuccess: false,
        isFailure: true,
        status: state.status);
  }

  factory NewProductState.success(NewProductState state) {
    return NewProductState(
        newProduct: state.newProduct,
        isLoading: false,
        isSuccess: true,
        isFailure: false,
        status: state.status);
  }

  get error => null;

  NewProductState update(
      {List<NewsProduct> newProduct,
      bool isLoading,
      bool isSuccess,
      bool isFailure,
      DioStatus status}) {
    return copyWith(
      newProduct: newProduct,
      isLoading: false,
      isSuccess: false,
      isFailure: false,
      status: status,
    );
  }

  NewProductState copyWith({
    List<NewsProduct> newProduct,
    bool isLoading,
    bool isSuccess,
    bool isFailure,
    DioStatus status,
  }) {
    return NewProductState(
      newProduct: newProduct ?? this.newProduct,
      isLoading: isLoading ?? this.isLoading,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      status: status ?? this.status,
    );
  }

  @override
  String toString() {
    return 'PostState{post: $newProduct, isLoading: $isLoading, isSuccess: $isSuccess, isFailure: $isFailure, status: $status}';
  }
}
