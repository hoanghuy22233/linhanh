import 'package:equatable/equatable.dart';

class NewProductEvent extends Equatable {
  const NewProductEvent();

  List<Object> get props => [];
}

class LoadNewProduct extends NewProductEvent {}

class RefreshNewProduct extends NewProductEvent {}

