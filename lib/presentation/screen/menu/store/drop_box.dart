import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
class DropBox extends StatefulWidget {
  
  final String value;
  
  const DropBox({Key key, this.value}) : super(key: key);
  @override
  _DropBoxState createState() => _DropBoxState();
}

class _DropBoxState extends State<DropBox> {
  @override
  Widget build(BuildContext context) {
    String dropdownValue = widget.value;
    return DropdownButton<String>(
      value: dropdownValue,
      icon: Icon(Icons.filter_alt_outlined),
      iconSize: 24,
      elevation: 16,
      isExpanded: true,
      underline: SizedBox(),
      style: TextStyle(color: Colors.black),
      onChanged: (String newValue) {
        setState(() {
          dropdownValue = newValue;
        });
      },
      items: <String>[dropdownValue]
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
